#-------------------------------------------------------------------------------
# curium app
# ALWAYS FOLLOW: https://docs.zephyrproject.org/latest/develop/application/index.html#application-cmakelists-txt


cmake_minimum_required(VERSION 3.20)

set(BOARD curiumobc)

find_package(Zephyr)

project(app LANGUAGES C CXX)

set(CMAKE_C_STANDARD 17)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD_REQUIRED True)
set(CMAKE_CXX_STANDARD_REQUIRED True)


target_sources(app PRIVATE src/main.cpp)
