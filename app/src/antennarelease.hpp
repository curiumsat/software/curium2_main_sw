#pragma once
// no lines before pragma once

#include <stdio.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>
#include "little_fs_handler.hpp"
#include <exception>
#include <stdexcept>

/**
 * @brief Initiates the antenna release sequence by activating burn wires.
 *
 * This function sequentially activates a series of burn wires to release the antenna.
 * It first enables the burn process by setting the burn enable pin high. Then, each burn wire
 * is activated for a fixed duration before being deactivated. The function also performs
 * a fuse check after each burn wire activation.
 * Note: Current sleep durations are placeholders and should be adjusted to final values.
 */

inline void antennarelease(int wdt_id, struct k_msgq &error_msgq)
{
    // printk("------------------------------");
    // printk("Antennarelease running \n");
    int bootcount_curium = 0;
    int error_to_q = 0;
    try
    {
        // Check if antennarelease has been performed previously
        bootcount_curium = little_fs_bootcount_read();
        // printk("Bootcount: %d\n", bootcount_curium);
        // make sure antennarelease happens at least three times as redundant measure
        if (bootcount_curium > 2)
        {
            // printk("Antennarelease performed previously\n");
            bootcount_curium = little_fs_bootcount_increase();
            // printk("Bootcount increased by one: %d\n", bootcount_curium);
            return;
        }
        // printk("Antennarelease not performed previously, starting wait.\n");
    }
    catch (const std::exception &e)
    {
        printk("Exception caught: %s\n", e.what());
        error_to_q = 5;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
        // ignoring exception, if no bootcounter was found continue with antenna release.
    }
    catch (...)
    {
        printk("Unknown exception caught\n");
        // ignoring exception, if no bootcounter was found continue with antenna release.
        error_to_q = 6;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
    }
    
    // sleep 30 minutes before antenna release, every 10 sec feed the watchdog
    for (int i = 0; i < 6 * 30; i++)
    {
        task_wdt_feed(wdt_id);
        printk("Antenna waiting\n");
        k_msleep(10000); 
    }


    static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(DT_ALIAS(led0), gpios);
    static const struct gpio_dt_spec burn_enable = GPIO_DT_SPEC_GET(DT_ALIAS(burnenable), gpios);
    static const struct gpio_dt_spec burn_fault = GPIO_DT_SPEC_GET(DT_ALIAS(burnfault), gpios);
    static const struct gpio_dt_spec burn_pins[] = {
        GPIO_DT_SPEC_GET(DT_ALIAS(burn1), gpios),
        GPIO_DT_SPEC_GET(DT_ALIAS(burn2), gpios),
        GPIO_DT_SPEC_GET(DT_ALIAS(burn4), gpios),
        GPIO_DT_SPEC_GET(DT_ALIAS(burn7), gpios),
        GPIO_DT_SPEC_GET(DT_ALIAS(burn5), gpios),
        GPIO_DT_SPEC_GET(DT_ALIAS(burn6), gpios),
        GPIO_DT_SPEC_GET(DT_ALIAS(burn0), gpios),
        GPIO_DT_SPEC_GET(DT_ALIAS(burn3), gpios)};

    // Fault condition shall be set true if a critical error occured, release will still be executed but bootcouter not increased
    bool fault_condition = false;
    int ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
    if (ret < 0)
    {
        printk("Error configuring GPIO device led. Ignoring Error.\n");
        error_to_q = 7;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
    }
    printk("Device led configured\n");

    ret = gpio_pin_configure_dt(&burn_enable, GPIO_OUTPUT_LOW);
    if (ret < 0)
    {
        printk("Error configuring GPIO device burn_enable\n");
        fault_condition = true;
        error_to_q = 8;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(&error_msgq,&ret,K_NO_WAIT);

    }
    printk("Device burn_enable configured\n");

    ret = gpio_pin_configure_dt(&burn_fault, GPIO_INPUT);
    if (ret < 0)
    {
        printk("Error configuring GPIO device burn_fault. Ignoring error.\n");
        error_to_q = 9;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
    }

    int burn_pin_number[8] = {1,2,4,7,5,6,0,3};
    int i = 0; // just for printout
    for (const auto pin : burn_pins)
    {
        printk("------------------------");
        printk("Antennarelease sequence for burn pin %d\n", burn_pin_number[i]);
        if (!gpio_is_ready_dt(&pin))
        {
            printk("Device burn%d not ready\n", burn_pin_number[i]);
            fault_condition = true;
            error_to_q = 10;
		    k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
        }

        ret = gpio_pin_configure_dt(&pin, GPIO_OUTPUT);
        if (ret < 0)
        {
            printk("Error configuring GPIO device burn%d\n", burn_pin_number[i]);
            fault_condition = true;
            error_to_q = 11;
		    k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
            k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
        }
        // Turn on Fuse
        gpio_pin_set_dt(&led, 1);
        ret = gpio_pin_set_dt(&burn_enable, 1);
        if (ret < 0)
        {
            printk("Error setting GPIO device burn_enable\n");
            fault_condition = true;
            error_to_q = 12;
		    k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
            k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
        } else {
            printk("Device burn_enable set high\n");
        }
        

        // Burn high
        ret = gpio_pin_set_dt(&pin, 1);
        if (ret < 0)
        {
            printk("Error setting GPIO device burn%d\n", burn_pin_number[i]);
            fault_condition = true;
            error_to_q = 13;
            k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
            k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
            i++;
            continue;
        }
        printk("--------------Device burn%d high\n", burn_pin_number[i]);

        task_wdt_feed(wdt_id);
        k_msleep(8000); // wait for to 8 seconds

        // Fuse check
        ret = gpio_pin_get_dt(&burn_fault);
        if (ret != 0)
        {
            printk("Fuse tripped or gpio device failure.\n");
            error_to_q = 14;
		    k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
            k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
            fault_condition = true;
        } else {
            printk("Fuse OK.");
        }

        // Burn low
        ret = gpio_pin_set_dt(&pin, 0);
        if (ret != 0)
        {
            printk("Error setting GPIO device burn%d\n", burn_pin_number[i]);
            error_to_q = 15;
		    k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
            k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
        } else {
            printk("Device burn%d low\n", burn_pin_number[i]);
        }

        // Fuse reset
        gpio_pin_set_dt(&led, 0);
        ret = gpio_pin_set_dt(&burn_enable, 0);
        if (ret != 0)
        {
            printk("Error setting GPIO device burn_enable\n");
            error_to_q = 16;
		    k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
            k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
            fault_condition = true;
        } else {
            printk("Device burn_enable set to 0\n");
        }
        task_wdt_feed(wdt_id);
        k_msleep(5000);

        i++;
    }

    if (fault_condition)
    {
        k_msleep(30000); // wait for error telemetry to arrive at satnogs
        printk("Antennarelease sequence failed\n");
        printk("Rebooting device, omitting bootcount increase...\n");
        sys_reboot(SYS_REBOOT_COLD);
    }

    task_wdt_feed(wdt_id);
    printk("Antennarelease sequence completed\n");
    int new_bootcount = little_fs_bootcount_increase();
    printk("Bootcount increased by one: %d\n", new_bootcount);

}
