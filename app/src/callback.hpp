#pragma once
/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <zephyr/kernel.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>

inline void
task_wdt_callback(int channel_id, void *user_data)
{
  printk("Task watchdog channel %d callback, thread: %s\n", channel_id,
         k_thread_name_get((k_tid_t)user_data));

  /*
   * If the issue could be resolved, call task_wdt_feed(channel_id) here
   * to continue operation.
   *
   * Otherwise we can perform some cleanup and reset the device.
   */
  // reset i2c bus with i2cnrst
  printk("Resetting i2c bus\n");
  static const struct gpio_dt_spec i2cnrst = GPIO_DT_SPEC_GET(DT_ALIAS(i2cnrst), gpios);  
  int ret = gpio_pin_configure_dt(&i2cnrst, GPIO_OUTPUT_LOW);
  if (ret < 0)
  {
    printk("Error configuring GPIO device i2cnrst\n");
  }
  gpio_pin_set_dt(&i2cnrst, 1);
  k_msleep(200);
  gpio_pin_set_dt(&i2cnrst, 0);




  printk("Resetting device...\n");
  sys_reboot(SYS_REBOOT_COLD);
}