#pragma once
//no lines before pragma once
// Checkk https://github.com/scttnlsn/bms/blob/master/src/bq769x0.c for battery management system driver

#include <stdio.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   2000



// Function to demonstrate I2C GPIO expansion test using Zephyr RTOS APIs.
inline int i2c_test() {
    
    printk("i2c_test running \n");
    k_msleep(100); 

    // Check if the I2C channel for EPS is enabled in the device tree.
    #if DT_NODE_HAS_STATUS(DT_ALIAS(i2c_channel_eps), okay)
    #define I2C_EPS_CTRL_NODE_ID      DT_ALIAS(i2c_channel_eps)
    #else
    #error "I2C 0 controller device not found" // Compilation error if I2C channel is not found.
    #endif

    // Define the node for PCA20P8 GPIO expander.
    #define PCA20P8_NODE DT_ALIAS(gpioext20p8)
    // Retrieve the device specification from the device tree.
    static const struct gpio_dt_spec pca20p8 = GPIO_DT_SPEC_GET(PCA20P8_NODE, gpios);

    // Get the I2C device from the device tree.
    const struct device *dev = DEVICE_DT_GET(DT_ALIAS(i2c_channel_eps));
	int ret_i2c;
	if (dev == NULL) {
		printk("Could not get i2c device\n"); // Log error if device is not found.
	}

    // Check if the GPIO device is ready to use.
	if (!gpio_is_ready_dt(&pca20p8)) {
        printk("Device pca20p8 not ready\n"); // Log error if device is not ready.
	}

    // Configure the GPIO pin as output and set it active.
	ret_i2c = gpio_pin_configure_dt(&pca20p8, GPIO_OUTPUT_ACTIVE);
	if (ret_i2c < 0) {
        printk("Failed to configure pca20p8\n"); // Log error if configuration fails.
	}

    // Loop to toggle the state of the GPIO pin.
	bool pca20p8_state = true;
    int i = 0;
	while (i < 1000) {
		ret_i2c = gpio_pin_toggle_dt(&pca20p8); // Toggle the GPIO pin.
		if (ret_i2c < 0) {
            printk("Failed to toggle pca20p8\n"); // Log error if toggle fails.
		}
		pca20p8_state = !pca20p8_state; // Update the state variable.
		printk("pca20p8 state toggle\n"); // Log the toggle action.
		k_msleep(SLEEP_TIME_MS); // Sleep for a defined time between toggles.
        i++;
	}
    return 0;
}


