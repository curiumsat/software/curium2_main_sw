#include <stdio.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/task_wdt/task_wdt.h>

struct data_battery_voltage {
    float bat_voltage = 0;
    float cell0 = 0;
    float cell1 = 0;
	float cell2 = 0;
	float cell3 = 0;
	float cell4 = 0;
	float battery_temp = 0;
}; 
struct solar_charge_values {
	// struct raw_voltages {float v1 = 0; float v2 = 0; float v3 = 0; float v4 = 0; float v5 = 0; float v6 = 0; float v7 = 0; float v8 = 0; float v9 = 0; };
	float max_currents_out[4]; 
	float max_solar_voltages_out[4]; 
	int32_t max_index_out[4];
};

// internal includes
#include "mpptracker.hpp"
#include "antennarelease.hpp"
// #include "i2c_test.hpp"
// #include "ltc2991_test.hpp"
#include "callback.hpp"
#include "housekeeping.hpp"
#include "batterymanager.hpp"



#if DT_HAS_COMPAT_STATUS_OKAY(st_stm32_watchdog)
#define WDT_NODE DT_COMPAT_GET_ANY_STATUS_OKAY(st_stm32_watchdog)
#else
#error "IWDG is not available. Check the device tree configuration"
#endif


K_MSGQ_DEFINE(bat_msgq, sizeof(struct data_battery_voltage), 2, 1);
K_MSGQ_DEFINE(mppt_msgq, sizeof(struct solar_charge_values), 2, 1);
K_MSGQ_DEFINE(error_msgq, sizeof(int), 100, 1);


K_THREAD_DEFINE(mpptracker_id, 1024*4, mpptracker, &mppt_msgq, &error_msgq, NULL, 5, 0, 1000);
K_THREAD_DEFINE(batterymanager_id, 1024*4, batterymanager, &bat_msgq, &error_msgq, NULL, 6, 0, 5000);
K_THREAD_DEFINE(housekeeping_id, 1024*4, housekeeping, &bat_msgq, &mppt_msgq, &error_msgq, 10, 0, 15000);



int main(void)
{
	// sleep for 2sec to avoid "bricking" the MCU  
	k_msleep(2000);

	// FIRST enable watchdog to govern all following code
	const struct device *hw_wdt_dev = DEVICE_DT_GET_OR_NULL(WDT_NODE);
	int error_to_q = 0;
	// Ignoring all errors to avoid reboot before antenna release has happened
	if (!device_is_ready(hw_wdt_dev))
	{
		printk("Hardware watchdog not ready; ignoring it.\n");
		error_to_q = 1;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
	}
	

	int ret = task_wdt_init(hw_wdt_dev);
	if (ret)
	{
		printk("task wdt init failure: %d\n", ret);
		error_to_q = 2;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
		k_msgq_put(&error_msgq,&ret,K_NO_WAIT);

	}

	// timeout of 40 minutes to make sure antennarelease works 
	int main_wdt_id = task_wdt_add(1000*60*40, task_wdt_callback,
							  (void *)k_current_get());
	/*
	 * task_wdt_add() does activates the IWDG internally. The IWDG is enabled at
	 * the first call of task_wdt_feed(). So it is essential for the
	 * task_wdt_feed() to be called before any code that may hang the MCU
	 */
	ret = task_wdt_feed(main_wdt_id);
	if (ret)
	{
		printk("task wdt feed failure: %d\n", ret);
		error_to_q = 3;
		k_msgq_put(&error_msgq,&error_to_q,K_NO_WAIT);
		k_msgq_put(&error_msgq,&ret,K_NO_WAIT);
	}



	printk("Hello - This is Curium!\n");
	antennarelease(main_wdt_id, error_msgq);
	while (1)
	{
		k_msleep(1000);
		
		task_wdt_feed(main_wdt_id);

	}
}
