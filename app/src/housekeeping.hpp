#pragma once

// #include <zephyr/devicetree.h>
// #include <zephyr/drivers/emul.h>
// #include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/sensor.h>
#include <syscalls/kernel.h>
#include <zephyr/task_wdt/task_wdt.h>

#include "callback.hpp"
#include "little_fs_handler.hpp"

#include <pb_encode.h>
#include <pb_decode.h>
#include "app/src/satnogs-curium.pb.h"
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/uart.h>


static SimpleMessage satnogs_message = SimpleMessage_init_zero;
static uint8_t buffer[SimpleMessage_size];

inline void housekeeping(struct k_msgq *bat_msgq, struct k_msgq *mppt_msgq, struct k_msgq *error_msgq)
{
    int const housekeeping_wdt_id = task_wdt_add(60000, task_wdt_callback, (void *)k_current_get());
    struct sensor_value temp_value_generalltc;
    struct sensor_value temp_value_sunsensorltc;
    struct sensor_value voltage_value_generalltc[9];
    struct sensor_value voltage_value_sunsensorltc[9];
    struct data_battery_voltage from_batterymanager;
    struct solar_charge_values from_mppt;

    int error_out[100];
    memset(&error_out, 0, sizeof(error_out));
    int error_out_old[100];
    memset(&error_out_old, 0, sizeof(error_out_old));
    int error_to_q = 0;

    int bootcount_out = little_fs_bootcount_read();

    const struct device *dev_generalltc = DEVICE_DT_GET(DT_ALIAS(generalltc2991));
    if (!dev_generalltc)
    {
        printk("Failed to get device binding\n");
        error_to_q = 301;
        k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
    }

    const struct device *dev_sunsensorltc = DEVICE_DT_GET(DT_ALIAS(sunsensorltc2991));
    if (!dev_sunsensorltc)
    {
        printk("Failed to get device binding\n");
        error_to_q = 302;
        k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
    }

    if (!device_is_ready(dev_generalltc))
    {
        printk("\nError: Device \"%s\" is not ready; "
               "check the driver initialization logs for errors.\n",
               dev_generalltc->name);

        error_to_q = 303;
        k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
    }

    if (!device_is_ready(dev_sunsensorltc))
    {
        printk("\nError: Device \"%s\" is not ready; "
               "check the driver initialization logs for errors.\n",
               dev_sunsensorltc->name);
        error_to_q = 315;
        k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
    }

    static const struct device *const uart_satnogs_comms_dev = DEVICE_DT_GET(DT_ALIAS(uart_comms));
    if (!uart_satnogs_comms_dev)
    {
        printk("Failed to get device binding\n");
        error_to_q = 304;
        k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
    }

    while (1)
    {
        task_wdt_feed(housekeeping_wdt_id);

        // fetch data
        int ret = sensor_sample_fetch(dev_generalltc);
        if (ret)
        {
            printk("Failed to fetch data from %s\n", dev_generalltc->name);
            error_to_q = 305;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }

        ret = sensor_sample_fetch(dev_sunsensorltc);
        if (ret)
        {
            printk("Failed to fetch data from %s\n", dev_sunsensorltc->name);
            error_to_q = 306;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }
        k_msleep(200);
        ret = sensor_sample_fetch_chan(dev_generalltc, SENSOR_CHAN_DIE_TEMP);
        if (ret < 0)
        {
            printk("Failed to fetch sensor data\n");
            error_to_q = 307;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }

        ret = sensor_sample_fetch_chan(dev_sunsensorltc, SENSOR_CHAN_DIE_TEMP);
        if (ret < 0)
        {
            printk("Failed to fetch sensor data\n");
            error_to_q = 308;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }
        k_msleep(200);

        // Get the temperature channel data
        ret = sensor_channel_get(dev_generalltc, SENSOR_CHAN_DIE_TEMP, &temp_value_generalltc);
        if (ret < 0)
        {
            printk("Failed to read temperature channel\n");
            error_to_q = 309;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }
        ret = sensor_channel_get(dev_sunsensorltc, SENSOR_CHAN_DIE_TEMP, &temp_value_sunsensorltc);
        if (ret < 0)
        {
            printk("Failed to read temperature channel\n");
            error_to_q = 310;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }
        ret = sensor_sample_fetch_chan(dev_generalltc, SENSOR_CHAN_VOLTAGE);
        if (ret < 0)
        {
            printk("Failed to fetch sensor data\n");
            error_to_q = 311;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }
        ret = sensor_sample_fetch_chan(dev_sunsensorltc, SENSOR_CHAN_VOLTAGE);
        if (ret < 0)
        {
            printk("Failed to fetch sensor data\n");
            error_to_q = 312;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }

        ret = sensor_channel_get(dev_generalltc, SENSOR_CHAN_VOLTAGE, voltage_value_generalltc);
        if (ret < 0)
        {
            printk("Failed to read voltage channel\n");
            error_to_q = 313;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }

        ret = sensor_channel_get(dev_sunsensorltc, SENSOR_CHAN_VOLTAGE, voltage_value_sunsensorltc);
        if (ret < 0)
        {
            printk("Failed to read voltage channel\n");
            error_to_q = 314;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
            k_msgq_put(error_msgq, &ret, K_NO_WAIT);
        }

        for (int i = 0; i < 9; i++)
        {
            printk("general Voltage: %d.%06d\n", voltage_value_generalltc[i].val1, voltage_value_generalltc[i].val2);
        }
        printk("general Temperature: %d.%06d\n", temp_value_generalltc.val1, temp_value_generalltc.val2);
        // for (int i = 0; i < 9; i++) {
        //     printk("solar Voltage: %d.%06d\n", voltage_value_solarltc[i].val1, voltage_value_solarltc[i].val2);
        // }
        // printk("solar Temperature: %d.%06d\n", temp_value_solarltc.val1, temp_value_solarltc.val2);
        for (int i = 0; i < 9; i++)
        {
            printk("sunsensor Voltage: %d.%06d\n", voltage_value_sunsensorltc[i].val1, voltage_value_sunsensorltc[i].val2);
        }
        printf("sunsensor Temperature: %f\n", sensor_value_to_float(&temp_value_sunsensorltc));

        k_msgq_get(bat_msgq, &from_batterymanager, K_MSEC(10000));

        printf("Battery Voltage: %f\n", from_batterymanager.bat_voltage);

        k_msgq_get(mppt_msgq, &from_mppt, K_MSEC(10000));
        printf("Max currents out: %f\n", from_mppt.max_currents_out[0]);

        printf("Bootcount: %d\n", bootcount_out);

        int empty_error = 0;

        memset(error_out, 0, sizeof(error_out));

        for (int i = 0; i < 5; i++)
        {
            if (0 != k_msgq_peek_at(error_msgq, &error_out[i], i))
            {
                error_out[i] = 0;
            }
            printk("Error: %d\n", error_out[i]);
        }

        while (k_msgq_num_used_get(error_msgq) >= 50)
        {
            k_msgq_get(error_msgq, &empty_error, K_NO_WAIT);
        }

        int64_t uptime_msec = k_uptime_get();
        printf("Uptime: %lld\n", uptime_msec);

        /* Allocate space on the stack to store the message data. */

        pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

        satnogs_message.has_battery_data = true;
        satnogs_message.battery_data.total_voltage = from_batterymanager.bat_voltage;
        satnogs_message.battery_data.cell0 = from_batterymanager.cell0;
        satnogs_message.battery_data.cell1 = from_batterymanager.cell1;
        satnogs_message.battery_data.cell2 = from_batterymanager.cell2;
        satnogs_message.battery_data.cell3 = from_batterymanager.cell3;
        satnogs_message.battery_data.cell4 = from_batterymanager.cell4;
        satnogs_message.battery_data.battery_temp = from_batterymanager.battery_temp;
        satnogs_message.has_temperatures = true;
        satnogs_message.temperatures.temp_generalltc = sensor_value_to_float(&temp_value_generalltc);
        satnogs_message.temperatures.temp_sunsensorltc = sensor_value_to_float(&temp_value_sunsensorltc);
        satnogs_message.has_vgeneral = true;
        satnogs_message.vgeneral.v_supply = sensor_value_to_float(&voltage_value_generalltc[0]);
        satnogs_message.vgeneral.v1 = sensor_value_to_float(&voltage_value_generalltc[1]);
        satnogs_message.vgeneral.v2 = sensor_value_to_float(&voltage_value_generalltc[2]);
        satnogs_message.vgeneral.v3 = sensor_value_to_float(&voltage_value_generalltc[3]);
        satnogs_message.vgeneral.v4 = sensor_value_to_float(&voltage_value_generalltc[4]);
        satnogs_message.vgeneral.v5 = sensor_value_to_float(&voltage_value_generalltc[5]);
        satnogs_message.vgeneral.v6 = sensor_value_to_float(&voltage_value_generalltc[6]);
        satnogs_message.vgeneral.v7 = sensor_value_to_float(&voltage_value_generalltc[7]);
        satnogs_message.vgeneral.v8 = sensor_value_to_float(&voltage_value_generalltc[8]);
        satnogs_message.has_vsunsensor = true;
        satnogs_message.vsunsensor.v_supply = sensor_value_to_float(&voltage_value_sunsensorltc[0]);
        satnogs_message.vsunsensor.v1 = sensor_value_to_float(&voltage_value_sunsensorltc[1]);
        satnogs_message.vsunsensor.v2 = sensor_value_to_float(&voltage_value_sunsensorltc[2]);
        satnogs_message.vsunsensor.v3 = sensor_value_to_float(&voltage_value_sunsensorltc[3]);
        satnogs_message.vsunsensor.v4 = sensor_value_to_float(&voltage_value_sunsensorltc[4]);
        satnogs_message.vsunsensor.v5 = sensor_value_to_float(&voltage_value_sunsensorltc[5]);
        satnogs_message.vsunsensor.v6 = sensor_value_to_float(&voltage_value_sunsensorltc[6]);
        satnogs_message.vsunsensor.v7 = sensor_value_to_float(&voltage_value_sunsensorltc[7]);
        satnogs_message.vsunsensor.v8 = sensor_value_to_float(&voltage_value_sunsensorltc[8]);
        satnogs_message.has_mppt_values = true;
        satnogs_message.mppt_values.maxcurrent0 = from_mppt.max_currents_out[0];
        satnogs_message.mppt_values.maxcurrent1 = from_mppt.max_currents_out[1];
        satnogs_message.mppt_values.maxcurrent2 = from_mppt.max_currents_out[2];
        satnogs_message.mppt_values.maxcurrent3 = from_mppt.max_currents_out[3];
        satnogs_message.mppt_values.maxvoltage0 = from_mppt.max_solar_voltages_out[0];
        satnogs_message.mppt_values.maxvoltage1 = from_mppt.max_solar_voltages_out[1];
        satnogs_message.mppt_values.maxvoltage2 = from_mppt.max_solar_voltages_out[2];
        satnogs_message.mppt_values.maxvoltage3 = from_mppt.max_solar_voltages_out[3];
        satnogs_message.mppt_values.maxindex0 = from_mppt.max_index_out[0];
        satnogs_message.mppt_values.maxindex1 = from_mppt.max_index_out[1];
        satnogs_message.mppt_values.maxindex2 = from_mppt.max_index_out[2];
        satnogs_message.mppt_values.maxindex3 = from_mppt.max_index_out[3];
        satnogs_message.bootcount = bootcount_out;
        satnogs_message.uptime_msec = uptime_msec;
        satnogs_message.has_error_arr = true;
        satnogs_message.error_arr.error0 = error_out[0];
        satnogs_message.error_arr.error1 = error_out[1];
        satnogs_message.error_arr.error2 = error_out[2];
        satnogs_message.error_arr.error3 = error_out[3];
        satnogs_message.error_arr.error4 = error_out[4];
        satnogs_message.error_arr.error5 = error_out[5];
        satnogs_message.error_arr.error6 = error_out[6];
        satnogs_message.error_arr.error7 = error_out[7];
        satnogs_message.error_arr.error8 = error_out[8];
        satnogs_message.error_arr.error9 = error_out[9];
        

        bool status = pb_encode_delimited(&stream, SimpleMessage_fields, &satnogs_message);
        size_t message_length = stream.bytes_written;

        if (!status)
        {
            printk("Encoding failed: %s\n", PB_GET_ERROR(&stream));
            error_to_q = 316;
            k_msgq_put(error_msgq, &error_to_q, K_NO_WAIT);
        }

        // Send encoded message over UART
        for (size_t i = 0; i < message_length; i++)
        {
            uart_poll_out(uart_satnogs_comms_dev, buffer[i]);
        }

        k_msleep(20000);
    }
}