#pragma once

#include <zephyr/devicetree.h>
#include <zephyr/drivers/emul.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/sensor.h>



inline int ltc2991_test() {
    const struct device *dev;
    struct sensor_value temp_value;
    struct sensor_value voltage_value[9];


    // Initialize the device (assuming the device name is known)
    dev = DEVICE_DT_GET(DT_ALIAS(generalltc2991));
    if (!dev) {
        printk("Failed to get device binding\n");
        return -1;
    }

	if (!device_is_ready(dev)) {
		printk("\nError: Device \"%s\" is not ready; "
		       "check the driver initialization logs for errors.\n",
		       dev->name);
		return -1;
	}
    printk("Found device \"%s\", getting sensor data\n", dev->name);

    // Fetch the sensor data
    if (sensor_sample_fetch_chan(dev,SENSOR_CHAN_DIE_TEMP) < 0) {
        printk("Failed to fetch sensor data\n");
        return -1;
    }

    // Get the temperature channel data
    if (sensor_channel_get(dev, SENSOR_CHAN_DIE_TEMP, &temp_value) < 0) {
        printk("Failed to read temperature channel\n");
        return -1;
    }

    // Print the temperature value
    printk("Temperature: %d.%06d\n", temp_value.val1, temp_value.val2);
    
    if (sensor_sample_fetch_chan(dev,SENSOR_CHAN_VOLTAGE) < 0) {
        printk("Failed to fetch sensor data\n");
        return -1;
    }
    if (sensor_channel_get(dev, SENSOR_CHAN_VOLTAGE, voltage_value) < 0) {
        printk("Failed to read voltage channel\n");
        return -1;
    }

    // print all voltage values
    for (int i = 0; i < 9; i++) {
        printk("Voltage: %d.%06d\n", voltage_value[i].val1, voltage_value[i].val2);
    }

    return 0;
}