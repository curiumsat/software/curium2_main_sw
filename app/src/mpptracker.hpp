#pragma once
// no lines before pragma once

#include <stdio.h>
#include <zephyr/kernel.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/drivers/dac.h>
// #include <zephyr/logging/log.h>
#include <zephyr/task_wdt/task_wdt.h>
#include "callback.hpp"

inline void mpptracker(struct k_msgq *mppt_msgq, struct k_msgq *error_msgq)
{
    k_msleep(3000);
    int error_to_q = 0;
    int ret = 0;
    int task_wdt_id = task_wdt_add(60000, task_wdt_callback, (void *)k_current_get());

    printk("mpptracker start \n");


    static struct dac_channel_cfg dac_ch_cfg = {
        .channel_id = 1,
        .resolution = 8,
        .buffered = true};

    const struct device *dac_dev = DEVICE_DT_GET(DT_ALIAS(solarad5354));
    if (!dac_dev)
    {
        printk("Failed to get device binding\n");
        error_to_q = 101;
		k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        k_msleep(1000*60*40);
        task_wdt_callback(task_wdt_id, (void *)k_current_get());
    }
    if (!device_is_ready(dac_dev))
    {
        printk("\nError: Device \"%s\" is not ready; "
               "check the driver initialization logs for errors.\n",
               dac_dev->name);
        error_to_q = 102;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);

        k_msleep(1000*60*40);
        task_wdt_callback(task_wdt_id, (void *)k_current_get());
    }
    
    int setup_ret = dac_channel_setup(dac_dev, &dac_ch_cfg);
    if (setup_ret < 0)
    {
        printk("Failed to setup dac channel\n");
        error_to_q = 103;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(error_msgq,&setup_ret,K_NO_WAIT);
    }

    dac_write_value(dac_dev, 1, 0);
    int dac_write_ret = 0;
    dac_write_ret = dac_write_value(dac_dev, 1, 155);
    if (dac_write_ret < 0)
    {
        printk("Failed to write dac value\n");
        error_to_q = 104;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(error_msgq,&dac_write_ret,K_NO_WAIT);
    }


    // Initialize the device (assuming the device name is known)
    const struct device *dev = DEVICE_DT_GET(DT_ALIAS(solarltc2991));
    if (!dev)
    {
        printk("Failed to get device binding solarltc2991 in mpptracker\n");
        error_to_q = 105;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        // set solar chargers to reasonable values
        for (size_t j = 0; j < 4; j++)
            {
                dac_write_ret = dac_write_value(dac_dev, j, (uint8_t)128);
                if (dac_write_ret < 0)
                {
                    printk("Failed to write dac value\n");
                    error_to_q = 106;
                    k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
                    k_msgq_put(error_msgq,&dac_write_ret,K_NO_WAIT);
                }
            }
        k_msleep(1000*60*50);
        task_wdt_callback(task_wdt_id, (void *)k_current_get());
    }

    if (!device_is_ready(dev))
    {
        printk("\nError: Device \"%s\" is not ready; "
               "check the driver initialization logs for errors.\n",
               dev->name);
        error_to_q = 107;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);

        for (size_t j = 0; j < 4; j++)
            {
                dac_write_ret = dac_write_value(dac_dev, j, (uint8_t)128);
                if (dac_write_ret < 0)
                {
                    printk("Failed to write dac value\n");
                    error_to_q = 108;
                    k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
                    k_msgq_put(error_msgq,&dac_write_ret,K_NO_WAIT);
                }
            }
        k_msleep(1000*60*50);
        task_wdt_callback(task_wdt_id, (void *)k_current_get());
    }

    // cycle through 256 values of the potentiometer
    struct sensor_value voltage_value[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

    while (1)
    {
        int max_index[4] = {0, 0, 0, 0};
        float max_current[4] = {0, 0, 0, 0};
        float max_voltage[4] = {0, 0, 0, 0};

        // 51, 85, 127
        for (size_t i = 0; i < 256; i = i + 51)
        {
            // printk("mpptracker %d\n", i);
            task_wdt_feed(task_wdt_id);
            // sprintk("mpptracker %d\n", i);
            // Set the potentiometer value to i
            for (size_t j = 0; j < 4; j++)
            {
                dac_write_ret = dac_write_value(dac_dev, j, (uint32_t)i);
                if (dac_write_ret < 0)
                {
                    printk("Failed to write dac value\n");
                    error_to_q = 109;
                    k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
                    k_msgq_put(error_msgq,&dac_write_ret,K_NO_WAIT);
                    continue;
                }
            }
            // wait for current normalization
            k_msleep(20);
            float current[4] = {0, 0, 0, 0};
            float voltage[4] = {0, 0, 0, 0};

            // Read back charge current from LTC2991
            ret = sensor_sample_fetch_chan(dev, SENSOR_CHAN_VOLTAGE);
            if (ret < 0)
            {
                printk("Failed to fetch sensor data\n");
                error_to_q = 110;
                k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
                continue;
            }

            ret = sensor_channel_get(dev, SENSOR_CHAN_VOLTAGE, voltage_value);
            if (ret < 0)
            {
                printk("Failed to read voltage channel\n");
                error_to_q = 111;
                k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
                k_msgq_put(error_msgq,&ret,K_NO_WAIT);
                continue;
            }

            for (size_t panel = 0; panel < 4; panel++)
            {
                int ltc_pin_current = (panel + 1) * 2 - 1;
                int ltc_pin_voltage = (panel + 1) * 2;
                current[panel] = voltage_value[ltc_pin_current].val1 + (float)voltage_value[ltc_pin_current].val2 / 1000000;
                current[panel] = current[panel] / 20 / (float)0.02;
                voltage[panel] = (voltage_value[ltc_pin_voltage].val1 + (float)voltage_value[ltc_pin_voltage].val2 / 1000000) * 11;

            }
            printf("Currents: %f %f %f %f\n", current[0], current[1], current[2], current[3]);
            printf("Voltages : %f %f %f %f \n", voltage[0], voltage[1], voltage[2], voltage[3]);
            // Compare current to max_current
            // if current is higher than max_current, set max_current to current and max_index to i
            for (size_t panel = 0; panel < 4; panel++)
            {
                if (max_current[panel] < current[panel])
                {
                    max_current[panel] = current[panel];
                    max_voltage[panel] = voltage[panel];
                    max_index[panel] = i;
                }
            }
        }
        for (size_t panel = 0; panel < 4; panel++)
        {
            dac_write_ret = dac_write_value(dac_dev, panel, (uint32_t)max_index[panel]);
            if (dac_write_ret < 0)
            {
                printk("Failed to write dac value\n");
                error_to_q = 112;
                k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
                k_msgq_put(error_msgq,&dac_write_ret,K_NO_WAIT);
            }
            printk("Setting potentiometer %d to %d\n", panel, max_index[panel]); // TODO: add max current
        }

        struct solar_charge_values output_mpptracker = {
            .max_currents_out = {max_current[0], max_current[1], max_current[2], max_current[3]},
            .max_solar_voltages_out = {max_voltage[0], max_voltage[1], max_voltage[2], max_voltage[3]},
            .max_index_out = {max_index[0], max_index[1], max_index[2], max_index[3]}
        };

        if (k_msgq_put(mppt_msgq, &output_mpptracker, K_NO_WAIT) != 0) {
            /* message queue is full: purge old data & try again */
            k_msgq_purge(mppt_msgq);
            k_msgq_put(mppt_msgq, &output_mpptracker, K_NO_WAIT);
        }

        k_msleep(5000);
    }
}
