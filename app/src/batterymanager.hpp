#pragma once

/*
 * Copyright (c) The Libre Solar Project Contributors
 *
 * SPDX-License-Identifier: Apache-2.0
 */


#include <zephyr/device.h>
#include <zephyr/kernel.h>

#include <stdio.h>

// #include "button.h"
// #include "data_objects.h"
#include "helper.h"
// #include "leds.h"
// #include "thingset.h"
#include <bms/bms.h>
#include "bms_common.c"
#include "bms_soc.c"

// LOG_MODULE_REGISTER(bms_main, CONFIG_LOG_DEFAULT_LEVEL);

struct bms_context bms = {
    .ic_dev = DEVICE_DT_GET(DT_ALIAS(bms_ic)),
};

void batterymanager(struct k_msgq *bat_msgq, struct k_msgq *error_msgq)
{
    int ret = 0;
    int error_to_q = 0;

    static const struct gpio_dt_spec battery_boot =  GPIO_DT_SPEC_GET(DT_ALIAS(bootbattery),gpios);
    ret = gpio_pin_configure_dt(&battery_boot, GPIO_OUTPUT_ACTIVE);
    if (ret < 0)
    {
        printk("Error configuring GPIO device battery_boot. Ignoring Error.\n");
        error_to_q = 207;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
    }
    printk("Device battery_boot configured\n");
    gpio_pin_set_dt(&battery_boot, 1);
    k_msleep(1000);
    gpio_pin_set_dt(&battery_boot, 0);


    // printk("Battery Manager started\n");
    if (!device_is_ready(bms.ic_dev)) {
        LOG_ERR("BMS IC not ready");
        error_to_q = 201;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        // return -ENODEV;
    }

    bms_ic_assign_data(bms.ic_dev, &bms.ic_data);

    ret = bms_ic_set_mode(bms.ic_dev, BMS_IC_MODE_ACTIVE);
    if (ret != 0) {
        LOG_ERR("Failed to activate BMS IC: %d", ret);
        error_to_q = 202;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(error_msgq,&ret,K_NO_WAIT);
    }

    ret = bms_ic_configure(bms.ic_dev, &bms.ic_conf, BMS_IC_CONF_ALL);
    if (ret < 0) {
        LOG_ERR("Failed to configure BMS IC: %d", ret);
        error_to_q = 203;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(error_msgq,&ret,K_NO_WAIT);
    }

    ret = bms_ic_read_data(bms.ic_dev, BMS_IC_DATA_CELL_VOLTAGES);
    if (ret != 0) {
        LOG_ERR("Failed to read data from BMS IC: %d", ret);
        error_to_q = 204;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(error_msgq,&ret,K_NO_WAIT);
    }

    ret = bms_ic_balance(bms.ic_dev,BMS_IC_BALANCING_AUTO);
    if (ret != 0) {
        LOG_ERR("Failed to start balancing: %d", ret);
        error_to_q = 205;
        k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
        k_msgq_put(error_msgq,&ret,K_NO_WAIT);
    }

    bms_soc_reset(&bms, -1);
    // printk("Battery Manager initialized\n");
    // button_init();

    int64_t t_start = k_uptime_get();
    while (true) {
        ret = bms_ic_read_data(bms.ic_dev, BMS_IC_DATA_ALL);
        if (ret != 0) {
            LOG_ERR("Failed to read data from BMS IC: %d", ret);
            error_to_q = 206;
            k_msgq_put(error_msgq,&error_to_q,K_NO_WAIT);
            k_msgq_put(error_msgq,&ret,K_NO_WAIT);
        }
        // printk("Battery Manager running\n");

        bms_soc_update(&bms);

        bms_state_machine(&bms);

        struct data_battery_voltage output_batterymanager = {
            .bat_voltage = bms.ic_data.total_voltage,
            .cell0 = bms.ic_data.cell_voltages[0],
            .cell1 = bms.ic_data.cell_voltages[1],
            .cell2 = bms.ic_data.cell_voltages[2],
            .cell3 = bms.ic_data.cell_voltages[3],
            .cell4 = bms.ic_data.cell_voltages[4],
            .battery_temp = bms.ic_data.cell_temps[0],
        };
        
        // printf("Voltage: %.2f V\n", bms.ic_data.total_voltage);
        if (k_msgq_put(bat_msgq, &output_batterymanager, K_NO_WAIT) != 0) {
            /* message queue is full: purge old data & try again */
            k_msgq_purge(bat_msgq);
            k_msgq_put(bat_msgq, &output_batterymanager, K_NO_WAIT);
        }

        t_start += CONFIG_BMS_IC_POLLING_INTERVAL_MS;
        k_sleep(K_TIMEOUT_ABS_MS(t_start));
    }
}

static int init_config(void)
{
    bms_init_config(&bms, (enum bms_cell_type)CONFIG_CELL_TYPE, CONFIG_BAT_CAPACITY_AH);

    return 0;
}

SYS_INIT(init_config, APPLICATION, 0);
