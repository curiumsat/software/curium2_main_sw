/*
 * SPDX-FileCopyrightText: Copyright (c) 2023 Carl Zeiss Meditec AG
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT adi_adltc2991

#include <zephyr/sys/util.h>
#include <zephyr/drivers/i2c.h>


#include "adltc2991_reg.h"
#include "adltc2991.h"

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(adltc2991, CONFIG_SENSOR_LOG_LEVEL);

// TODO: NECESSARY, updated to ltc2991
static bool adltc2991_is_busy(const struct device *dev)
{
	const struct adltc2991_config *cfg = dev->config;
	uint8_t status_reg = 0;

	i2c_reg_read_byte_dt(&cfg->bus, ADLTC2991_REG_TRIGGER, &status_reg);
	return status_reg & BIT(2);
}

// TODO: NECESSARY, updated to ltc2991
static int adltc2991_trigger_measurement(const struct device *dev)
{
	const struct adltc2991_config *cfg = dev->config;
	int ret;

	// k_msleep(10);
	ret = i2c_reg_write_byte_dt(&cfg->bus, ADLTC2991_REG_TRIGGER, 0xf8);
	if (ret < 0)
	{
		LOG_ERR("Failed to trigger measurement (error %d)", ret);
		return ret;
	}

	return 0;
}

// TODO: NECESSARY, updated to ltc2991
static int32_t adltc2991_get_value(const struct device *dev,
								   enum adltc2991_monitoring_type type,
								   enum adltc2991_monitor_pins pin)
{
	const struct adltc2991_config *cfg = dev->config;

	uint8_t msb_value = 0, lsb_value = 0;
	uint8_t msb_address, lsb_address;

	switch (pin)
	{
	case V1:
	{
		msb_address = ADLTC2991_REG_V1_MSB;
		lsb_address = ADLTC2991_REG_V1_LSB;
		break;
	}
	case V2:
	{
		msb_address = ADLTC2991_REG_V2_MSB;
		lsb_address = ADLTC2991_REG_V2_LSB;
		break;
	}
	case V3:
	{
		msb_address = ADLTC2991_REG_V3_MSB;
		lsb_address = ADLTC2991_REG_V3_LSB;
		break;
	}
	case V4:
	{
		msb_address = ADLTC2991_REG_V4_MSB;
		lsb_address = ADLTC2991_REG_V4_LSB;
		break;
	}
	case V5:
	{
		msb_address = ADLTC2991_REG_V5_MSB;
		lsb_address = ADLTC2991_REG_V5_LSB;
		break;
	}
	case V6:
	{
		msb_address = ADLTC2991_REG_V6_MSB;
		lsb_address = ADLTC2991_REG_V6_LSB;
		break;
	}
	case V7:
	{
		msb_address = ADLTC2991_REG_V7_MSB;
		lsb_address = ADLTC2991_REG_V7_LSB;
		break;
	}
	case V8:
	{
		msb_address = ADLTC2991_REG_V8_MSB;
		lsb_address = ADLTC2991_REG_V8_LSB;
		break;
	}
	case INTERNAL_TEMPERATURE:
	{
		msb_address = ADLTC2991_REG_INTERNAL_TEMP_MSB;
		lsb_address = ADLTC2991_REG_INTERNAL_TEMP_LSB;
		break;
	}
	case SUPPLY_VOLTAGE:
	{
		msb_address = ADLTC2991_REG_VCC_MSB;
		lsb_address = ADLTC2991_REG_VCC_LSB;
		break;
	}
	default:
	{
		LOG_ERR("Trying to access illegal register");
		return -EINVAL;
	}
	}
	adltc2991_trigger_measurement(dev);

	k_msleep(20);

	i2c_reg_read_byte_dt(&cfg->bus, msb_address, &msb_value);
	k_msleep(2);
	i2c_reg_read_byte_dt(&cfg->bus, lsb_address, &lsb_value);
	uint16_t conversion_factor;
	uint8_t negative_bit_index;

	if (type == VOLTAGE_SINGLEENDED)
	{
		conversion_factor = ADLTC2991_VOLTAGE_SINGLEENDED_CONVERSION_FACTOR;
		negative_bit_index = 14;
	}
	else if (type == TEMPERATURE)
	{
		conversion_factor = ADLTC2991_TEMPERATURE_CONVERSION_FACTOR;
		negative_bit_index = 12;
	}
	else
	{
		LOG_ERR("unknown type");
		return -EINVAL;
	}

	int16_t value = (msb_value << 8) + lsb_value;

	int32_t voltage_value_get = (value << (31 - negative_bit_index)) >> (31 - negative_bit_index);

	return (voltage_value_get * conversion_factor) / 100;
}

// TODO: NECESSARY
static int adltc2991_init(const struct device *dev)
{
	const struct adltc2991_config *cfg = dev->config;

	if (!i2c_is_ready_dt(&cfg->bus))
	{
		LOG_ERR("I2C bus %s not ready", cfg->bus.bus->name);
		return -ENODEV;
	}

	// hardcode the control register setting to 0
	const uint8_t ctrl_reg_setting_v1234 = 0;
	k_msleep(10);
	LOG_DBG("Setting Control Register 1234 to: 0x%x", ctrl_reg_setting_v1234);
	int err = i2c_reg_write_byte_dt(&cfg->bus, ADLTC2991_REG_CONTROL_V1234, ctrl_reg_setting_v1234);

	if (err < 0)
	{
		LOG_ERR("configuring for single bus failed: %d", err);
		return err;
	}

	const uint8_t ctrl_reg_setting_v5678 = 0;
	// LOG_DBG("Setting Control Register 5678 to: 0x%x", ctrl_reg_setting_v1234);
	k_msleep(20);
	err = i2c_reg_write_byte_dt(&cfg->bus, ADLTC2991_REG_CONTROL_V5678, ctrl_reg_setting_v5678);

	if (err < 0)
	{
		LOG_ERR("configuring for single bus failed: %d", err);
		return err;
	}
	LOG_INF("Initializing ADLTC2991 with name %s", dev->name);
	return 0;
}

// TODO: NECESSARY
static int adltc2991_sample_fetch(const struct device *dev, enum sensor_channel chan)
{
	struct adltc2991_data *data = dev->data;
	// unused: const struct adltc2991_config *cfg = dev->config;
	
	if (adltc2991_is_busy(dev))
		{
			LOG_INF("ADLTC2991 conversion ongoing");
			return -EBUSY;
		}

	// adltc2991_init(dev);
	// k_msleep(10);
	switch (chan)
	{
	case SENSOR_CHAN_DIE_TEMP:
	{
		
		// LOG_DBG("Fetching temperature data");
		// k_msleep(10);
		data->internal_temperature =
			adltc2991_get_value(dev, TEMPERATURE, INTERNAL_TEMPERATURE);
		break;
	}

	case SENSOR_CHAN_VOLTAGE:
	{
		
		// LOG_DBG("Fetching voltage data");
		data->supply_voltage =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, SUPPLY_VOLTAGE) +
			2500000;

		data->pins_voltages[0] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V1);

		data->pins_voltages[1] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V2);

		data->pins_voltages[2] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V3);

		data->pins_voltages[3] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V4);

		data->pins_voltages[4] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V5);

		data->pins_voltages[5] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V6);

		data->pins_voltages[6] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V7);

		data->pins_voltages[7] =
			adltc2991_get_value(dev, VOLTAGE_SINGLEENDED, V8);

		break;
	}
	case SENSOR_CHAN_ALL:
	{
		
		adltc2991_trigger_measurement(dev);
		break;
	}
	default:
	{
		LOG_ERR("does not measure channel: %d", chan);
		return -ENOTSUP;

		return 0;
	}
	}
	return 0;
}

// TODO: NECESSARY
static int adltc2991_channel_get(const struct device *dev, enum sensor_channel chan,
								 struct sensor_value *val)
{
	if (val == NULL)
	{
		LOG_ERR("Argument of type sensor_value* cannot be null ");
		return -EINVAL;
	}
	struct adltc2991_data *data = dev->data;

	// unused: uint8_t num_values_v1_v2 = 0, num_values_v3_v4 = 0;
	// unused:  uint8_t offset_index = 0;

	switch (chan)
	{
	case SENSOR_CHAN_DIE_TEMP:
	{
		val->val1 = (data->internal_temperature) / 10000;
		val->val2 = (data->internal_temperature) % 10000;
		LOG_DBG("Internal Temperature Value is:%d.%d", val->val1, val->val2);
		break;
	}
	case SENSOR_CHAN_VOLTAGE:
	{
		val[0].val1 = data->supply_voltage / 1000000;
		val[0].val2 = data->supply_voltage % 1000000;
		
		for (int i = 0; i < 8; i++)
		{
			val[i + 1].val1 = data->pins_voltages[i] / 1000000;
			val[i + 1].val2 = data->pins_voltages[i] % 1000000;
		}
		break;
	}
	default:
	{
		return -ENOTSUP;
	}
	}

	return 0;
}

static const struct sensor_driver_api adltc2991_driver_api = {
	.sample_fetch = adltc2991_sample_fetch,
	.channel_get = adltc2991_channel_get,
};

// gets config data from device tree

#define ADLTC2991_DEFINE(inst)                                                       \
	static struct adltc2991_data adltc2991_data_##inst;                              \
	static const struct adltc2991_config adltc2991_config_##inst = {                 \
		.bus = I2C_DT_SPEC_INST_GET(inst),                                           \
		.temp_format = DT_INST_PROP(inst, temperature_format),                       \
		.acq_format = DT_INST_PROP(inst, acquistion_format)};                        \
                                                                                     \
	SENSOR_DEVICE_DT_INST_DEFINE(inst, adltc2991_init, NULL, &adltc2991_data_##inst, \
								 &adltc2991_config_##inst, POST_KERNEL,              \
								 CONFIG_SENSOR_INIT_PRIORITY, &adltc2991_driver_api);

DT_INST_FOREACH_STATUS_OKAY(ADLTC2991_DEFINE)