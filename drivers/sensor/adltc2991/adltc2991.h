/*
 * SPDX-FileCopyrightText: Copyright (c) 2023 Carl Zeiss Meditec AG
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef ZEPHYR_DRIVERS_SENSOR_ADLTC2991_H
#define ZEPHYR_DRIVERS_SENSOR_ADLTC2991_H

#include <zephyr/types.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/sensor.h>

enum adltc2991_monitor_pins {
	V1,
	V2,
	V3,
	V4,
    V5,
    V6,
    V7,
    V8,
	INTERNAL_TEMPERATURE,
	SUPPLY_VOLTAGE
} adltc2991_monitor_pins;

enum adltc2991_monitoring_type {
	NOTHING,
	VOLTAGE_SINGLEENDED,
	TEMPERATURE
} adltc2991_monitoring_type;


struct pins_configuration {
	uint32_t pins_current_resistor;
};

struct adltc2991_data {
	int32_t internal_temperature;
	int32_t supply_voltage;
	int32_t pins_voltages[8];
    //possibly two more int32_t arrays like this:
    
};

struct adltc2991_config {
	struct i2c_dt_spec bus;
	uint8_t temp_format;
	uint8_t acq_format;
	// uint8_t measurement_mode[2];
	struct pins_configuration pins_v1_v2;
	struct pins_configuration pins_v3_v4;
    //possibly two more structs like this:
    struct pins_configuration pins_v5_v6;
    struct pins_configuration pins_v7_v8;
};

#endif /* ZEPHYR_DRIVERS_SENSOR_ADLTC2991_H */