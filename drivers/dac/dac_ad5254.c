/*
 * Copyright 2021 Grinn
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT adi_ad5254

#include <zephyr/kernel.h>
#include <zephyr/drivers/dac.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/logging/log.h>


LOG_MODULE_REGISTER(dac_ad5254, CONFIG_DAC_LOG_LEVEL);

#define AD5254_MAX_CHANNEL			4U
#define AD5254_RESOLUTION			0x8
#define AD5254_DAC_MAX_VAL			((1U << AD5254_RESOLUTION) - 1U)
#define AD5254_MULTI_WRITE_CMD_VAL		8U
#define AD5254_MULTI_WRITE_CMD_POS		3U
#define AD5254_MULTI_WRITE_CHANNEL_POS		1U
#define AD5254_MULTI_WRITE_REFERENCE_POS	7U
#define AD5254_MULTI_WRITE_POWER_DOWN_POS	5U
#define AD5254_MULTI_WRITE_POWER_DOWN_MASK	0X3
#define AD5254_MULTI_WRITE_GAIN_POS		4U
#define AD5254_MULTI_WRITE_DAC_UPPER_VAL_POS	8U
#define AD5254_MULTI_WRITE_DAC_UPPER_VAL_MASK	0xF
#define AD5254_MULTI_WRITE_DAC_LOWER_VAL_MASK	0xFF

struct ad5254_config {
	struct i2c_dt_spec bus;
	uint8_t power_down[AD5254_MAX_CHANNEL];
	uint8_t voltage_reference[AD5254_MAX_CHANNEL];
	uint8_t gain[AD5254_MAX_CHANNEL];
};

static int ad5254_channel_setup(const struct device *dev,
				const struct dac_channel_cfg *channel_cfg)
{
	if (channel_cfg->channel_id >= AD5254_MAX_CHANNEL) {
		return -ENOTSUP;
	}

	if (channel_cfg->resolution != AD5254_RESOLUTION) {
		return -ENOTSUP;
	}

	return 0;
}

static int ad5254_write_value(const struct device *dev, uint8_t channel, uint32_t value)
{
	const struct ad5254_config *config = (struct ad5254_config *)dev->config;
	// unused: uint8_t tx_data[3];
	int ret;

	if (channel >= AD5254_MAX_CHANNEL) {
		return -ENOTSUP;
	}

	if (value > AD5254_DAC_MAX_VAL) {
		return -ENOTSUP; 
	}
	k_msleep(40);
	LOG_DBG("Writing value %d to channel %d", value, channel);
    ret = i2c_reg_write_byte_dt(&config->bus, channel, value);
	if (ret < 0) {
		LOG_ERR("Failed to write data to device: %d", ret);
		return ret;
	}
	uint8_t check_value;
	i2c_reg_read_byte_dt(&config->bus, channel, &check_value);
	if (check_value != value) {
		LOG_ERR("Failed to write data to device: %d", ret);
		return -EIO;
	}

	return 0;
}

static int dac_ad5254_init(const struct device *dev)
{
	const struct ad5254_config *config = dev->config;

	if (!device_is_ready(config->bus.bus)) {
		LOG_ERR("%s device not found", config->bus.bus->name);
		return -ENODEV;
	}
	return 0;
}

static const struct dac_driver_api ad5254_driver_api = {
	.channel_setup = ad5254_channel_setup,
	.write_value = ad5254_write_value,
};

#define INST_DT_AD5254(index)							\
	static const struct ad5254_config ad5254_config_##index = {		\
		.bus = I2C_DT_SPEC_INST_GET(index),				\
		.power_down = DT_INST_PROP(index, power_down_mode),		\
		.voltage_reference = DT_INST_PROP(index, voltage_reference),	\
		.gain = DT_INST_PROP_OR(index, gain, {0}),			\
	};									\
										\
	DEVICE_DT_INST_DEFINE(index, dac_ad5254_init, NULL, NULL,		\
				&ad5254_config_##index,			\
				POST_KERNEL,					\
				CONFIG_DAC_AD5254_INIT_PRIORITY,		\
				&ad5254_driver_api);

DT_INST_FOREACH_STATUS_OKAY(INST_DT_AD5254);